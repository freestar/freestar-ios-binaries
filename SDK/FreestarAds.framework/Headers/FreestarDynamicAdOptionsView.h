//
//  FreestarDynamicAdOptionsView.h
//  test_App
//
//  Created by Lev Trubov on 12/2/20.
//  Copyright © 2020 Freestar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FreestarDynamicAdOptionsView : UIView

@end

NS_ASSUME_NONNULL_END
