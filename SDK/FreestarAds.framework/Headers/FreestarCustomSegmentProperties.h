//
//  FreestarCustomSegmentProperties.h
//  FreestarAds
//
//  Copyright © 2020 Freestar. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FreestarCustomSegmentProperties : NSObject

+(void)setCustomSegmentProperty:(NSString *)key with:(NSString *)value;
+(NSString *)getCustomSegmentProperty:(NSString *)key;
+(void)deleteCustomSegmentProperty:(NSString *)key;
+(void)deleteAllCustomSegmentProperties;
+(NSDictionary<NSString *, NSString*> *)getAllCustomSegmentProperties;

@end

NS_ASSUME_NONNULL_END
