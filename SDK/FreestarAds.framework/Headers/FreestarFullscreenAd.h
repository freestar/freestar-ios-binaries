//
//  FreestarFullscreenAd.h
//  FreestarAds
//
//  Copyright © 2020 Freestar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FreestarAds/FreestarConstants.h>

NS_ASSUME_NONNULL_BEGIN

@class FreestarInterstitialAd;

@protocol FreestarInterstitialDelegate
@optional
- (void)freestarInterstitialLoaded:(FreestarInterstitialAd *)ad;
- (void)freestarInterstitialFailed:(FreestarInterstitialAd *)ad because:(FreestarNoAdReason)reason;
- (void)freestarInterstitialShown:(FreestarInterstitialAd *)ad;
- (void)freestarInterstitialClicked:(FreestarInterstitialAd *)ad;
- (void)freestarInterstitialClosed:(FreestarInterstitialAd *)ad;
@end

@class FreestarRewardedAd;

@protocol FreestarRewardedDelegate
@optional
- (void)freestarRewardedLoaded:(FreestarRewardedAd *)ad;
- (void)freestarRewardedFailed:(FreestarRewardedAd *)ad because:(FreestarNoAdReason)reason;
- (void)freestarRewardedShown:(FreestarRewardedAd *)ad;
- (void)freestarRewardedClosed:(FreestarRewardedAd *)ad;
- (void)freestarRewardedFailedToStart:(FreestarRewardedAd *)ad because:(FreestarNoAdReason)reason;
- (void)freestarRewardedAd:(FreestarRewardedAd *)ad received:(NSString *)rewardName amount:(NSInteger)rewardAmount;
@end

@interface FreestarReward : NSObject

+ (instancetype) blankReward;

@property NSUInteger rewardAmount;
@property(copy) NSString *rewardName;
@property(copy) NSString *userID;
@property(copy) NSString *secretKey;

@end

@interface FreestarFullscreenAd : NSObject

- (void)selectPartners:(NSArray *)partnerList;
- (void)showFrom:(UIViewController *)viewController;
- (NSString *)winningPartnerName;
- (void)loadPlacement:(nullable NSString *)placement;

@property(copy) NSString *placement;

- (void)addCustomTargeting:(NSString *)key as:(NSString *)target;
- (void)removeCustomTargeting:(NSString *)key as:(NSString *)target;
- (void)removeAllCustomTargeting;

@end

@interface FreestarInterstitialAd : FreestarFullscreenAd

- (instancetype)initWithDelegate:(nullable id<FreestarInterstitialDelegate>)delegate;
@property(weak) id<FreestarInterstitialDelegate> _Nullable delegate;
@property(nonatomic, assign, getter=isAppOpenAd) BOOL appOpenAd;

@end

@interface FreestarRewardedAd : FreestarFullscreenAd

- (instancetype)initWithDelegate:(nullable id<FreestarRewardedDelegate>)delegate andReward:(FreestarReward *)reward;

@property(weak) id<FreestarRewardedDelegate> _Nullable delegate;
@property FreestarReward *reward;

@end

NS_ASSUME_NONNULL_END
