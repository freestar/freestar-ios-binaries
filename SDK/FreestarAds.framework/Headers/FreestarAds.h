//
//  FreestarAds.h
//  test_App
//
//  Copyright © 2020 Freestar. All rights reserved.

//

#ifndef FreestarAds_h
#define FreestarAds_h

#import <FreestarAds/FreestarConstants.h>
#import <FreestarAds/Freestar.h>
#import <FreestarAds/FreestarInformation.h>
#import <FreestarAds/FreestarCustomSegmentProperties.h>
#import <FreestarAds/FreestarBannerAd.h>
#import <FreestarAds/FreestarPrerollAd.h>
#import <FreestarAds/FreestarFullscreenAd.h>
#import <FreestarAds/FreestarNativeAd.h>
#import <FreestarAds/FreestarDynamicNativeAdView.h>
#import <FreestarAds/FreestarDynamicNativeMediaView.h>
#import <FreestarAds/FreestarDynamicAdOptionsView.h>

#endif /* FreestarAds_h */
