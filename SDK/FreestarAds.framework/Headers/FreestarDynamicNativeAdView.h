//
//  FreestarDynamicNativeAdView.h
//  test_App
//
//  Created by Lev Trubov on 12/2/20.
//  Copyright © 2020 Freestar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FreestarAds/FreestarDynamicNativeMediaView.h>
#import <FreestarAds/FreestarDynamicAdOptionsView.h>
//#import "FreestarDynamicNativeMediaView.h"
//#import "FreestarDynamicAdOptionsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreestarDynamicNativeAdView : UIView

@property(weak) IBOutlet FreestarDynamicNativeMediaView *iconView;
@property(weak) IBOutlet UILabel *headlineView;
@property(weak) IBOutlet UILabel *advertiserView;
@property(weak) IBOutlet FreestarDynamicAdOptionsView *adChoicesView;
@property(weak) IBOutlet FreestarDynamicNativeMediaView *mediaView;
@property(weak) IBOutlet UILabel *bodyView;
@property(weak) IBOutlet UILabel *socialContext;
@property(weak) IBOutlet UIButton *callToActionView;
@property(weak) IBOutlet UIImageView *logoView;

@property(weak) IBOutlet UILabel *storeView;
@property(weak) IBOutlet UILabel *priceView;

@end

NS_ASSUME_NONNULL_END
