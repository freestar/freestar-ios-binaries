//
//  FreestarNativeAd.h
//  test_App
//
//  Created by Lev Trubov on 11/12/20.
//  Copyright © 2020 Freestar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FreestarAds/FreestarConstants.h>

NS_ASSUME_NONNULL_BEGIN

@class FreestarNativeAd;

@protocol FreestarNativeAdDelegate
@optional
-(void)freestarNativeLoaded:(FreestarNativeAd *)ad;
-(void)freestarNativeFailed:(FreestarNativeAd *)ad because:(FreestarNoAdReason)reason;
-(void)freestarNativeShown:(FreestarNativeAd *)ad;
-(void)freestarNativeClicked:(FreestarNativeAd *)ad;
-(void)freestarNativeClosed:(FreestarNativeAd *)ad;
@end


@interface FreestarNativeAd : UIView

-(instancetype)initWithDelegate:(nullable id<FreestarNativeAdDelegate>)delegate andSize:(FreestarNativeAdSize)size;
-(void)loadPlacement:(nullable NSString *)placement;
-(void)show;

@property(weak) id<FreestarNativeAdDelegate> _Nullable delegate;
@property FreestarNativeAdSize size;


-(NSString *)winningPartnerName;

-(void)addCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeAllCustomTargeting;


@end

NS_ASSUME_NONNULL_END
