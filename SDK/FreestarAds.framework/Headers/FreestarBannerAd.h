//
//  FreestarBannerAd.h
//  FreestarAds
//
//  Copyright © 2020 Freestar. All rights reserved.

//

#import <UIKit/UIKit.h>
#import <FreestarAds/FreestarConstants.h>

NS_ASSUME_NONNULL_BEGIN

@class FreestarBannerAd;

@protocol FreestarBannerAdDelegate
@optional
-(void)freestarBannerLoaded:(FreestarBannerAd *)ad;
-(void)freestarBannerFailed:(FreestarBannerAd *)ad because:(FreestarNoAdReason)reason;
-(void)freestarBannerShown:(FreestarBannerAd *)ad;
-(void)freestarBannerClicked:(FreestarBannerAd *)ad;
-(void)freestarBannerClosed:(FreestarBannerAd *)ad;
@end

@interface FreestarBannerAd : UIView

-(instancetype)initWithDelegate:(nullable id<FreestarBannerAdDelegate>)delegate andSize:(FreestarBannerAdSize)size;
-(void)loadPlacement:(nullable NSString *)placement;
-(void)show;

@property(weak) id<FreestarBannerAdDelegate> _Nullable delegate;
@property FreestarBannerAdSize size __attribute__((deprecated("Pass size to initWithDelegate:andSize:")));
@property BOOL fixedSize;
@property CGFloat adaptiveBannerWidth;
@property(nonatomic, assign, readonly, getter=isAdaptive) BOOL adaptive;


-(NSString *)winningPartnerName;

-(void)addCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeAllCustomTargeting;

@end

NS_ASSUME_NONNULL_END
