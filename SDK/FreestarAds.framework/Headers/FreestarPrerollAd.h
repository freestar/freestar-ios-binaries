//
//  FreestarPrerollAd.h
//  FreestarAds
//
//  Copyright © 2020 Freestar. All rights reserved.

//

#import <UIKit/UIKit.h>
#import <FreestarAds/FreestarConstants.h>
@import AVKit;

NS_ASSUME_NONNULL_BEGIN

@class FreestarPrerollAd;

@protocol FreestarPrerollAdDelegate
@optional
-(void)freestarPrerollLoaded:(FreestarPrerollAd *)ad;
-(void)freestarPrerollFailed:(FreestarPrerollAd *)ad because:(FreestarNoAdReason)reason;
-(void)freestarPrerollShown:(FreestarPrerollAd *)ad;
-(void)freestarPrerollClicked:(FreestarPrerollAd *)ad;
-(void)freestarPrerollClosed:(FreestarPrerollAd *)ad;
-(void)freestarPrerollFailedToStart:(FreestarPrerollAd *)ad because:(FreestarNoAdReason)reason;

@end

//loads and plays automatically when added to the view hierarchy
//conforms to existing player's boundaries when playOver: method is used
@interface FreestarPrerollAd : NSObject
@property(weak) id<FreestarPrerollAdDelegate> _Nullable delegate;
@property(copy) NSString *placement;
@property FreestarPrerollAdSize size;

-(instancetype)initWithDelegate:(nullable id<FreestarPrerollAdDelegate>) delegate;
-(void)loadPlacement:(nullable NSString *)placement;
-(void)playOver:(AVPlayerViewController *)video;
-(void)playIn:(UIView *)view at:(CGPoint)position;

-(NSString *)winningPartnerName;

-(void)addCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeAllCustomTargeting;

@end

NS_ASSUME_NONNULL_END
