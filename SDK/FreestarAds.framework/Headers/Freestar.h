//
//  Freestar.h
//  FreestarPlugin
//
//  Copyright © 2020 Freestar. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <FreestarAds/FreestarInformation.h>
//#import "FreestarInformation.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FreestarMediationServingMode) {
    FreestarMediationServingModeDefault = 0,
    FreestarMediationServingModeBypassAll,
    FreestarMediationServingModeAdmobGam,
};

typedef NS_ENUM(NSUInteger, FreestarAppOpenAdEvent) {
    FreestarAppOpenAdEventInitial = 0,
    FreestarAppOpenAdEventLoading,
    FreestarAppOpenAdEventDidLoad,
    FreestarAppOpenAdEventWillShow,
    FreestarAppOpenAdEventDismissed,
    FreestarAppOpenAdEventFailed
};

@interface Freestar : NSObject

+ (FreestarDemographics *)demographics;
+ (FreestarLocation *)location;

+ (NSString *)appKey;
+ (void)initWithAppKey:(NSString *)appKey;

+ (NSString *)getAdUnitID __attribute__((deprecated("Use appKey instead")));
+ (void)initWithAdUnitID:(NSString *)apiKey __attribute__((deprecated("Use initWithAppKey: instead")));
+ (void)requestAppOpenAdsWithPlacement:(NSString *_Nonnull)placementId
                            waitScreen:(BOOL)waitScreen
                            completion:(void (^)(NSString *placement, FreestarAppOpenAdEvent event, NSError *_Nullable error))completion;

+ (FreestarMediationServingMode)servingMode;
+ (void)setServingMode:(FreestarMediationServingMode)mode;
+ (void)setTestModeEnabled:(BOOL)testModeEnabled;
+ (void)setLoggingEnabled:(BOOL)loggingEnabled;
+ (void)setAdaptiveBannerEnabledIfAvailable:(BOOL)adaptiveBannerEnabled;

+ (BOOL)loggingEnabled;
+ (BOOL)testModeEnabled;
+ (BOOL)adaptiveBannerEnabled;


@end

NS_ASSUME_NONNULL_END
