//
//  FreestarConstants.h
//  FreestarAds
//
//  Copyright © 2020 Freestar. All rights reserved.
//

#ifndef FreestarConstants_h
#define FreestarConstants_h

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, FreestarFullscreenAdState) {
    FreestarFullscreenAdStateLoading,
    FreestarFullscreenAdStateReadyToLoad,
    FreestarFullscreenAdStateFailedToLoad
};

typedef NS_ENUM(NSUInteger, FreestarNoAdReason) {
    FreestarNoAdReasonUnknown,
    FreestarNoAdReasonNoFill,
    FreestarNoAdReasonNetworkError,
    FreestarNoAdReasonCappedOrPaced,
    FreestarNoAdReasonPlaybackError,
    FreestarNoAdReasonInternalError,
    FreestarNoAdReasonSDKInitializationInProgress,
    FreestarNoAdReasonSDKInitializationError
};

typedef NS_ENUM(NSUInteger, FreestarBannerAdSize) {
    FreestarBanner300x250,
    FreestarBanner320x50,
    FreestarBanner728x90,
    FreestarBanner320x480 //Used for fullscreen        
};

typedef NS_ENUM(NSUInteger, FreestarPrerollAdSize) {
    FreestarPrerollFullscreen,
    FreestarPrerollMREC
};

typedef NS_ENUM(NSUInteger, FreestarNativeAdSize) {
    FreestarNativeSmall,
    FreestarNativeMedium
};

#endif /* FreestarConstants_h */
