//
//  FreestarInformation.h
//  FreestarPlugin
//
//  Copyright © 2020 Freestar. All rights reserved.

//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    FreestarGenderUnknown,
    FreestarGenderMale,
    FreestarGenderFemale,
    FreestarGenderOther
} FreestarGender;

typedef enum {
    FreestarMaritalStatusUnknown,
    FreestarMaritalStatusSingle,
    FreestarMaritalStatusMarried,
    FreestarMaritalStatusDivorced,
    FreestarMaritalStatusWidowed,
    FreestarMaritalStatusSeparated,
    FreestarMaritalStatusOther
} FreestarMaritalStatus;

@interface FreestarDemographics : NSObject

@property FreestarGender gender;
@property FreestarMaritalStatus maritalStatus;
@property NSUInteger age;
@property NSString *ethnicity;

-(void)setBirthdayYear:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day;

@end

@interface FreestarLocation : NSObject

@property CLLocation *location;
@property NSString *dmacode;
@property NSString *currpostal;
@property NSString *postalcode;
@property NSString *metro;
@property NSString *geo;
@property NSString *geoType;

@end
