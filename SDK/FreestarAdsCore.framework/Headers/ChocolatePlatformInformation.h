//
//  ChocolatePlatformInformation.h
//  test_App
//
//  Created by Lev Trubov on 3/8/18.
//  Copyright © 2018 Chocolate Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    ChocolatePlatformGenderUnknown,
    ChocolatePlatformGenderMale,
    ChocolatePlatformGenderFemale,
    ChocolatePlatformGenderOther
} ChocolatePlatformGender;

typedef enum {
    ChocolatePlatformMaritalStatusUnknown,
    ChocolatePlatformMaritalStatusSingle,
    ChocolatePlatformMaritalStatusMarried,
    ChocolatePlatformMaritalStatusDivorced,
    ChocolatePlatformMaritalStatusWidowed,
    ChocolatePlatformMaritalStatusSeparated,
    ChocolatePlatformMaritalStatusOther
} ChocolatePlatformMaritalStatus;

@interface ChocolatePlatformDemographics : NSObject

@property ChocolatePlatformGender gender;
@property ChocolatePlatformMaritalStatus maritalStatus;
@property NSUInteger age;
@property(copy) NSString *ethnicity;

-(void)setBirthdayYear:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day;

@end

@interface ChocolatePlatformLocation : NSObject


@property (strong) CLLocation *location;
@property (nonatomic, copy) NSString *dmacode;
@property (nonatomic, copy) NSString *currpostal;
@property (nonatomic, copy) NSString *postalcode;
@property (nonatomic, copy) NSString *metro;
@property (nonatomic, copy) NSString *geo;
@property (nonatomic, copy) NSString *geoType;

@end

@interface ChocolatePlatformAppInfo : NSObject

@property (nonatomic, strong) NSArray *keywords;
@property (nonatomic, copy) NSString *requester;
@property (nonatomic, copy) NSString *appBundle;
@property (nonatomic, copy) NSString *appDomain;
@property (nonatomic, copy) NSString *appName;
@property (nonatomic, copy) NSString *appStoreUrl;
@property (nonatomic, copy) NSString *Category;
@property (nonatomic, copy) NSString *publisherdomain;
@property double totalInAppPurchaseAmount;

@end

@interface ChocolatePlatformPrivacySettings : NSObject

-(void) subjectToGDPR:(BOOL)gdprApplies withConsent:(NSString *)gdprConsentString;

@end
