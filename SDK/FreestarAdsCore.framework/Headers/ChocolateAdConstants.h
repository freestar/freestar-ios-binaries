//
//  ChocolateAdConstants.h
//  test_App
//
//  Created by Lev Trubov on 10/29/19.
//  Copyright © 2019 Chocolate Platform. All rights reserved.
//

#ifndef ChocolateAdConstants_h
#define ChocolateAdConstants_h

typedef NS_ENUM(NSUInteger, ChocolateAdLoadingState) {
    ChocolateAdLoading,
    ChocolateAdReadyToLoad,
    ChocolateAdFailedToLoad
};

typedef NS_ENUM(NSUInteger, ChocolateAdNoAdReason) {
    ChocolateAdUnknown,
    ChocolateAdNoFill,
    ChocolateAdNetworkError,
    ChocolateAdCappedOrPaced,
    ChocolateAdPlaybackError,
    ChocolateAdInternalError,
    ChocolateAdReasonSDKInitializationInProgress,
    ChocolateAdReasonSDKInitializationError
};

typedef NS_ENUM(NSUInteger, ChocolateBannerAdSize) {
    ChocolateBanner300x250,
    ChocolateBanner320x50,
    ChocolateBanner728x90,
    ChocolateBanner320x480 //Used for fullscreen
};

typedef NS_ENUM(NSUInteger, ChocolatePrerollAdSize) {
    ChocolatePrerollFullscreen,
    ChocolatePrerollMREC
};

typedef NS_ENUM(NSUInteger, ChocolateNativeAdSize) {
    ChocolateNativeSmall,
    ChocolateNativeMedium
};

#endif /* ChocolateAdConstants_h */
