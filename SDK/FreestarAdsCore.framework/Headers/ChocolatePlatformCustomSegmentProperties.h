//
//  ChocolatePlatformCustomSegmentProperties.h
//  test_App
//
//  Created by Lev Trubov on 9/27/18.
//  Copyright © 2018 Chocolate Platform. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChocolatePlatformCustomSegmentProperties : NSObject

+(void)setCustomSegmentProperty:(NSString *)key with:(NSString *)value;
+(NSString *)getCustomSegmentProperty:(NSString *)key;
+(void)deleteCustomSegmentProperty:(NSString *)key;
+(void)deleteAllCustomSegmentProperties;
+(NSDictionary<NSString *, NSString*> *)getAllCustomSegmentProperties;

@end

NS_ASSUME_NONNULL_END
